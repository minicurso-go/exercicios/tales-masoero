package main

import "fmt"

// Faça um algoritmo que leia um número inteiro positivo e mostre todos os seus divisores.

func main() {
	var num int

	fmt.Print("Digite um numero: ")
	fmt.Scan(&num)

	for i := num; i >= 1; i-- {
		if num%i == 0 {
			fmt.Println(i)
		}
	}
}
