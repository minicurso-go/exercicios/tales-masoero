package main

import "fmt"

// Faça um algoritmo que imprima a tabuada de multiplicação de 1 a 10 para um número fornecido pelo usuário.

func main() {
	var num int

	fmt.Print("Escolha um número: ")
	fmt.Scan(&num)

	for i := 1; i < 11; i++ {
		fmt.Printf("%dx%d=%d\n", num, i, num*i)
	}
}
