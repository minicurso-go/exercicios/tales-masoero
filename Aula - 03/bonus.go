package main

import "fmt"

// Faça um programa para verificar se uma matriz 3x3 forma um quadrado mágico. Quadrado Mágico é uma tabela quadrada
// igual a intersecção de números em que a soma de cada coluna, de cada linha e das duas diagonais são iguaisEntrada
// A entrada consiste de 9 linhas. Cada linha contém um inteiro. Os 9 inteiros representam uma matriz 3x3 onde os
// três primeiros inteiros representam os valores da primeira linha.Saída. Imprima "SIM" se a matriz forma
// um quadrado mágico ou imprima "NAO" caso contrário.

func main() {
	var matrix [3][3]int

	matrix = fillMatrix(matrix)
	if isMagic := checkMagicSquare(matrix); isMagic {
		fmt.Println("SIM!")
	} else {
		fmt.Println("NAO!")
	}
}

func fillMatrix(m [3][3]int) [3][3]int {
	fmt.Println("Preencha a matiz:")

	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			fmt.Scan(&m[i][j])
		}
	}
	return m
}

func checkRows(m [3][3]int) bool {
	for i := 0; i < len(m); i++ {
		sumRows := 0
		for j := 0; j < len(m[i]); j++ {
			sumRows += m[i][j]
		}
		if sumRows != 15 {
			return false
		}
	}
	return true
}

func checkColumns(m [3][3]int) bool {
	for i := 0; i < len(m); i++ {
		sumColumns := 0
		for j := 0; j < len(m[i]); j++ {
			sumColumns += m[j][i]
		}
		if sumColumns != 15 {
			return false
		}
	}
	return true
}

func checkMagicSquare(m [3][3]int) bool {
	rows := checkRows(m)
	columns := checkColumns(m)
	return rows && columns
}
