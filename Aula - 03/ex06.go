package main

import (
	"fmt"
	"math"
)

// Faça um algoritmo que leia vários números inteiros e mostre o maior deles.
// A leitura deve ser interrompida quando for lido o valor zero.

func main() {
	var num, biggestNum int
	biggestNum = math.MinInt

	for {
		fmt.Print("Numero: ")
		fmt.Scan(&num)

		if num == 0 {
			fmt.Print("O maior numero foi: ", biggestNum)
			break
		}

		if num > biggestNum {
			biggestNum = num
		}
	}
}
