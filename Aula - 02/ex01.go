package main

import "fmt"

// Leia 2 valores com uma casa decimal (x e y), que devem representar as coordenadas de um ponto em um plano.
// A seguir, determine qual o quadrante ao qual pertence o ponto, ou se está sobre um dos eixos cartesianos ou
// na origem  (x = y = 0). Se o ponto estiver na origem, escreva a mensagem “Origem”. Se o ponto estiver sobre
// um dos eixos escreva “Eixo X” ou “Eixo Y”, conforme for a situação.

func main() {
	x, y := getValues()
	switch {
	case x > 0 && y > 0:
		fmt.Print("Está no primeiro quadrante")
	case x > 0 && y < 0:
		fmt.Print("Está no quarto quadrante")
	case x < 0 && y > 0:
		fmt.Print("Está no segundo quadrante")
	case x < 0 && y < 0:
		fmt.Print("Está no terceiro quadrante")
	case x == 0 && y == 0:
		fmt.Print("Está na origem")
	case x == 0:
		fmt.Print("Está no eixo Y")
	default:
		fmt.Print("Está no eixo x")
	}
}

func getValues() (float64, float64) {
	var x, y float64

	fmt.Print("Valor de X: ")
	fmt.Scan(&x)
	fmt.Print("Valor de Y: ")
	fmt.Scan(&y)

	return x, y
}
