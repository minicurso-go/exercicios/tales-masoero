package main

import "fmt"

// Roberto tem um conjunto de lápis com 10 tons diferentes de uma mesma cor, numerados de 0 a 9. Numa fita quadriculada,
// alguns quadrados foram coloridos inicialmente com o tom 0. Roberto precisa determinar, para cada quadrado Q não colorido,
// qual é a distância dele para o quadrado mais próximo de tom 0. A distância entre dois quadrados é definida com
// o número mínimo de movimentos para a esquerda, ou para a direita, para ir de um quadrado para o outro. O quadrado Q,
// então, deve ser colorido com o tom cuja numeração corresponde à distância determinada. Se a distância
// for maior ou igual a 9, o quadrado deve ser colorido com o tom 9. Seu programa deve colorir e imprimir
// a fita quadriculada dada na entrada.
//
// Entrada:
//
// A primeira linha da entrada contém apenas um inteiro N, indicando o número de quadrados da fita. A segunda linha
// contém N números inteiros: “-1” se o quadrado não está colorido, e “0” se está colorido com o tom 0.
//
// Saída:
//
// Seu programa deve escrever na saída a fita totalmente colorida, de acordo com a regra definida acima.

func main() {
	var sliceColored []int
	arrColored := [...]int{-1, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1}

	for i := 0; i < len(arrColored); i++ {
		if arrColored[i] == -1 {
			sliceBack := arrColored[:i+1]
			sliceFront := arrColored[i:]
			cBack := closestBack(sliceBack)
			cFront := closestFront(sliceFront)
			sliceColored = append(sliceColored, min(cBack, cFront))
		} else {
			sliceColored = append(sliceColored, arrColored[i])
		}
	}
	fmt.Print(sliceColored)
}

func closestBack(sBack []int) int {
	var closest = 100

	for i := len(sBack) - 1; i >= 0; i-- {
		if sBack[i] == 0 {
			closest = (len(sBack) - 1) - i
			break
		}
	}
	return closest
}

func closestFront(sFront []int) int {
	var closest = 100

	for i := 0; i < len(sFront); i++ {
		if sFront[i] == 0 {
			closest = i
			break
		}
	}
	return closest
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
