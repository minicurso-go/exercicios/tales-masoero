package main

import (
	"fmt"
	"math"
)

// Fazer um programa que leia três valores com ponto flutuante de dupla precisão: A, B e C. Em seguida, calcule e mostre:
//
// a) a área do triângulo retângulo que tem A por base e C por altura.
// b) a área do círculo de raio C. (pi = 3.14159).
// c) a área do trapézio que tem A e B por bases e C por altura.
// d) a área do quadrado que tem lado B.
// e) a área do retângulo que tem lados A e B.

func main() {
	var a, b, c float64
	pi := math.Pi

	fmt.Print("Digite 3 números: ")
	fmt.Scan(&a, &b, &c)

	fmt.Println("a) ", (a*c)/2)
	fmt.Println("b) ", pi*(c*c))
	fmt.Println("c) ", ((a+b)*c)/2)
	fmt.Println("d) ", b*b)
	fmt.Println("e) ", a*b)
}
