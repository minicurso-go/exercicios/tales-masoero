package main

import "fmt"

// Fazer um programa para ler quatro valores inteiros A, B, C e D. A seguir, calcule e mostre a diferença do produto de C e D segundo a fórmula: DIFERENÇA = (A * B - C * D).
//
// Entrada:                 Saída:
//  5                             Diferença = -26
//  6
//  7
//  8

func main() {
	var a, b, c, d int

	fmt.Print("Digite 4 números: ")
	fmt.Scan(&a, &b, &c, &d)

	fmt.Print("DIFERENÇA = ", a*b-c*d)
}
