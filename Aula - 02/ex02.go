package main

import "fmt"

// Escreva um algoritmo que solicita ao usuário dois operandos e um código de operação
// (1 - Soma, 2 - Subtração, 3 - Divisão ou 4 - Multiplicação) e realiza a
// operação correspondente sobre os operandos fornecidos pelo usuário:

func main() {
	var n1, n2 float64
	var op int

	fmt.Print("Primeiro numero: ")
	fmt.Scan(&n1)
	fmt.Print("Segundo numero: ")
	fmt.Scan(&n2)
	fmt.Print("Operacao: ")
	fmt.Scan(&op)

	switch op {
	case 1:
		fmt.Printf("A soma eh: %.2f", n1+n2)
	case 2:
		fmt.Printf("A subtracao eh: %.2f", n1-n2)
	case 3:
		fmt.Printf("A divisao eh: %.2f", n1/n2)
	default:
		fmt.Printf("A multiplicacao eh: %.2f", n1*n2)
	}
}
