package main

import (
	"fmt"
)

// Joãozinho mora em uma rua que tem N casas. Marquinhos é o melhor amigo dele, mas sempre gosta de pregar peças em
// Joãozinho. Desta vez, ele pegou os dois brinquedos prediletos de Joãozinho e os escondeu em duas casas distintas
// da rua. Em compensação, Marquinhos deu uma dica importante para Joãozinho: A soma dos números das casas em que
// escondi teus brinquedos é igual a K. Além disso, escolhi as casas de tal forma que não existe outro par de casas
// cuja soma tenha esse mesmo valor. Sabendo disto, encontre qual é o par de casas em que se encontram os brinquedos
// de Joãozinho. Para auxiliar seu amigo, Marquinhos entregou a Joãozinho uma lista com o número das casas já em
// ordem crescente (isto é, do menor para o maior número).

// Entrada: A primeira primeira linha da entrada contém um número inteiro N, que representa o número de casas que
// existem na rua. Cada uma das N linhas seguintes contém um número inteiro, representando o número de uma casa.
// Note que esses N números estão ordenados, do menor para o maior. A última linha da entrada contém um inteiro K,
// que é a soma dos números das duas casas onde os brinquedos estão escondidos.

// Saída: Seu programa deve imprimir uma única linha, contendo dois inteiros, A e B, A<B, que representam os números
// das casas em que estão escondidos os brinquedos. Os dois números devem ser separados por um espaço em branco.

var arrHouses = [...]int{1, 2, 3, 5}
var hSum = 8

func main() {
	pairsMap := mapHouses(arrHouses)
	houses := getHousesNum(pairsMap, hSum)
	printHouses(houses)

}

func printHouses(h [2]int) {
	fmt.Printf("Casas: %d - %d", h[0], h[1])
}

func getHousesNum(pMap map[[2]int]int, sum int) [2]int {
	for key, val := range pMap {
		if val == sum {
			return key
		}
	}
	return [2]int{0, 0}
}

func mapHouses(arr [len(arrHouses)]int) map[[2]int]int {
	pairsMap := make(map[[2]int]int)

	for i := 1; i < len(arr); i++ {
		// pegando um array contendo o numero de cada casa como chave e sua soma como valor
		pairsMap[[2]int{arr[i-1], arr[i]}] = arr[i-1] + arr[i]
	}
	return pairsMap
}
