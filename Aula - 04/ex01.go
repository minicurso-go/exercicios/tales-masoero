package main

import "fmt"

// Usando função faça um algoritmo simples que pede dois números como entrada e retorna a soma desses dois números.

func main() {
	fmt.Println(sum(15, 7))
}

func sum(a, b int) int {
	return a + b
}
