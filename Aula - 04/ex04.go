package main

import "fmt"

// Usando função faça um algoritmo que calcula o fatorial de um número, ou seja,
// o produto de todos os números inteiros de 1 até aquele número.

func main() {
	x := fatorial(5)
	fmt.Print(x)
}

func fatorial(num int) int {
	var fat = num

	for i := num - 1; i > 0; i-- {
		fat *= i
	}
	return fat
}
