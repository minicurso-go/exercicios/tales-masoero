package main

import "fmt"

// Usando função faça um algoritmo que converte uma temperatura em Celsius para Fahrenheit ou vice-versa.

func main() {
	celsiusFahrenheit(20)
	fahrenheitCelsius(66)
}

func celsiusFahrenheit(c float64) {
	fmt.Println((c * 1.8) + 32)
}

func fahrenheitCelsius(f float64) {
	fmt.Println((f - 32) / 1.8)
}
