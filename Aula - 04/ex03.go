package main

import "fmt"

// Usando função faça um algoritmo que calcula a média de uma lista de números.

func main() {
	calculateMedia(1, 2, 3, 4, 5, 6, 7, 8, 9)
	calculateMedia(546, 65.6546, 765.2, 0, 1000, 0, 0)
}

func calculateMedia(num ...float64) {
	var total float64

	for _, n := range num {
		total += n
	}

	media := total / float64(len(num))
	fmt.Printf("A média de %.2f é %.2f\n", num, media)
}
