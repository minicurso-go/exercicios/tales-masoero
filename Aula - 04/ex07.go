package main

import "fmt"

// Usando função faça um algoritmo que verifica se um número é primo,
//ou seja, se é divisível apenas por 1 e por ele mesmo.

func main() {
	fmt.Println(isPrime(2))
	fmt.Println(isPrime(10))
	fmt.Println(isPrime(29))
	fmt.Println(isPrime(49))
	fmt.Println(isPrime(100))
}

func isPrime(n int) bool {
	if n == 0 || n == 1 {
		return false
	}

	for i := 2; i < n; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}
