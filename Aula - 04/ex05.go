package main

import (
	"errors"
	"fmt"
)

// Usando função faça um algoritmo que procura por um determinado elemento em uma lista,
// verificando cada elemento da lista até encontrá-lo.

func main() {
	n, err := findNumber(25, 10, 11, 12, 14, 15, 20, 100, 17, 22, 25)
	if err != nil {
		fmt.Print(err)
	} else {
		fmt.Printf("Numero %d encontrado!", n)
	}
}

// o primeiro numero da lista é o que vai ser procurado
func findNumber(n int, numbers ...int) (int, error) {
	for _, num := range numbers {
		if n == num {
			return n, nil
		}
	}
	return 0, errors.New("Numero nao encontrado!")
}
