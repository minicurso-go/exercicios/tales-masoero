package main

import "fmt"

// Usando função faça um algoritmo que calcula a potência de um número, elevando-o a uma determinada potência.

func main() {
	pow(2, 10)
}

func pow(num, pot int) {
	var result = num
	for i := 1; i < pot; i++ {
		result *= num
	}
	fmt.Print(result)
}
